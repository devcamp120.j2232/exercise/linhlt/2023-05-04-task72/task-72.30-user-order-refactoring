package com.devcamp.task6070jparelationshipordercustomer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task6070jparelationshipordercustomer.model.CUser;

public interface IUserRepository extends JpaRepository<CUser, Long>{
    CUser findById(long id);
    CUser findByOrdersId(long id);
}
