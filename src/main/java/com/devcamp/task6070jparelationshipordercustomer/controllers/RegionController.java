package com.devcamp.task6070jparelationshipordercustomer.controllers;
import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.task6070jparelationshipordercustomer.model.CCountry;
import com.devcamp.task6070jparelationshipordercustomer.model.CRegion;
import com.devcamp.task6070jparelationshipordercustomer.repository.CountryRepository;
import com.devcamp.task6070jparelationshipordercustomer.repository.RegionRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class RegionController {
    @Autowired
	private RegionRepository regionRepository;
	
	@Autowired
	private CountryRepository countryRepository;

	@GetMapping("/regions")
	public ResponseEntity<List<CRegion>> getAllRegions(@RequestParam(value = "page", defaultValue = "1") String page,
	@RequestParam(value = "size", defaultValue = "5") String size) {
		try {
			List<CRegion> regionList = regionRepository.findAll();
			return new ResponseEntity<>(regionList, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	//get all regions by country
	@GetMapping("/countries/{countryId}/regions")
    public ResponseEntity<List<CRegion>> getRegionsByCountry(@PathVariable(value = "countryId") Long countryId) {
		try {
			List<CRegion> regionList = regionRepository.findByCountryId(countryId);
			if (regionList != null){
				return new ResponseEntity<>(regionList, HttpStatus.OK);
			}
			else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }

	//get region info by region Id
	@GetMapping("/regions/{id}")
	public ResponseEntity<CRegion> getRegionById(@PathVariable Long id) {
		try {
			Optional<CRegion> region = regionRepository.findById(id);
			if (region.isPresent()){
				return new ResponseEntity<>(region.get(), HttpStatus.OK);
			}
			else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	//get region info of a country
	@GetMapping("/countries/{countryId}/regions/{id}")
    public ResponseEntity<CRegion> getRegionByRegionAndCountry(@PathVariable(value = "countryId") Long countryId,@PathVariable(value = "id") Long regionId) {
		try {
			Optional<CRegion> region = regionRepository.findByIdAndCountryId(regionId,countryId);
			if (region.isPresent()){
				return new ResponseEntity<>(region.get(), HttpStatus.OK);
			}
			else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
	//create region of country
	@PostMapping("/countries/{id}/regions")
	public ResponseEntity<Object> createRegion(@PathVariable("id") Long id, @Valid @RequestBody CRegion cRegion) {
		try {
			Optional<CCountry> countryData = countryRepository.findById(id);
			if (countryData.isPresent()) {
			CRegion newRegion = new CRegion();
			newRegion.setRegionName(cRegion.getRegionName());
			newRegion.setRegionCode(cRegion.getRegionCode());
			//newRegion.setCountry(cRegion.getCountry());
			
			CCountry _country = countryData.get();
			newRegion.setCountry(_country);
			newRegion.setCountryName(_country.getCountryName());
			
			CRegion savedRole = regionRepository.save(newRegion);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
			}
			else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified region: "+e.getCause().getCause().getMessage());
		}
	}

	@PutMapping("/countries/{countryId}/regions/{id}")
	public ResponseEntity<Object> updateRegion(@PathVariable(value = "countryId") Long countryId, @PathVariable("id") Long id, @Valid @RequestBody CRegion cRegion) {
		try {
			Optional<CCountry> country = countryRepository.findById(countryId);
			Optional<CRegion> regionData = regionRepository.findByIdAndCountryId(id, countryId);
			if (regionData.isPresent()) {
				CRegion newRegion = regionData.get();
				newRegion.setRegionName(cRegion.getRegionName());
				newRegion.setRegionCode(cRegion.getRegionCode());
				newRegion.setCountry(country.get());
				CRegion savedRegion = regionRepository.save(newRegion);
				return new ResponseEntity<>(savedRegion, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to update specified region: "+e.getCause().getCause().getMessage());
		}
	}

	@DeleteMapping("/countries/{countryId}/regions/{id}")
	public ResponseEntity<Object> deleteRegionById(@PathVariable(value = "countryId") Long countryId, @PathVariable Long id) {
		try {
			Optional<CRegion> regionData = regionRepository.findByIdAndCountryId(id, countryId);
			if (regionData.isPresent()){
				regionRepository.delete(regionData.get());
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	
	//task 7
	@GetMapping("/countries/{countryId}/regions-count")
    public long countRegionByCountryId(@PathVariable(value = "countryId") Long countryId) {
		List <CRegion> regions = regionRepository.findByCountryId(countryId);
        return regions.size();
    }
	
	//task 8
	@GetMapping("/regions/check/{id}")
	public boolean checkRegionById(@PathVariable Long id) {
		return regionRepository.existsById(id);
	}
	//Task 70.30
	@GetMapping("/region/code/{code}")
	public CRegion getRegionByCode(@PathVariable String code) {
		return regionRepository.findByRegionCode(code);
	}

	@GetMapping("/region/name/{name}")
	public CRegion getRegionByName(@PathVariable String name) {
		return regionRepository.findByRegionName(name);
	}
	
	//get region by region code and country code
	@GetMapping("/country1/{countryId}/regions1/{id}")
    public Optional<CRegion> getRegionByRegionCAndCountryC(@PathVariable(value = "countryId") String countryId,@PathVariable(value = "id") String regionId) {
        return regionRepository.findByRegionCodeAndCountryCountryCode(regionId,countryId);
    }

	@GetMapping("/regions5")
	public ResponseEntity<List<CRegion>> getFiveRegions(@RequestParam(value = "page", defaultValue = "1") String page,
	@RequestParam(value = "size", defaultValue = "5") String size) {
		try {
			Pageable pageWithFiveElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
			List<CRegion> regionList = new ArrayList<CRegion>();
			regionRepository.findAll(pageWithFiveElements).forEach(regionList::add);;
			return new ResponseEntity<>(regionList, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
