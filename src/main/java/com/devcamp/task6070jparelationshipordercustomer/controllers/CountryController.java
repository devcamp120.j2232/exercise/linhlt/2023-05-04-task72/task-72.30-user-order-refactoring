package com.devcamp.task6070jparelationshipordercustomer.controllers;
import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.task6070jparelationshipordercustomer.model.CCountry;
import com.devcamp.task6070jparelationshipordercustomer.repository.CountryRepository;

@RestController
@CrossOrigin
@RequestMapping("/")

public class CountryController {
    @Autowired
	private CountryRepository countryRepository;
	
	@GetMapping("/countries")
	public ResponseEntity<List<CCountry>> getAllCountries(@RequestParam(value = "page", defaultValue = "1") String page,
	@RequestParam(value = "size", defaultValue = "5") String size) {
		try {
			List<CCountry> countryList = countryRepository.findAll();
			return new ResponseEntity<>(countryList, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/countries/{id}")
	public ResponseEntity<CCountry> getCountryById(@PathVariable Long id) {
		try {
			Optional<CCountry> country = countryRepository.findById(id);
			if (country.isPresent()){
				return new ResponseEntity<>(country.get(), HttpStatus.OK);
			}
			else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/countries")
	public ResponseEntity<Object> createCountry(@Valid @RequestBody CCountry cCountry) {
		try {
			CCountry country = new CCountry();
			country.setCountryName(cCountry.getCountryName());
			country.setCountryCode(cCountry.getCountryCode());
			country.setRegions(cCountry.getRegions());
			CCountry savedRole = countryRepository.save(country);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified Country: "+e.getCause().getCause().getMessage());
		}
	}

	@PutMapping("/countries/{id}")
	public ResponseEntity<Object> updateCountry(@PathVariable("id") Long id, @Valid @RequestBody CCountry cCountry) {
		try {
			Optional<CCountry> countryData = countryRepository.findById(id);
			if (countryData.isPresent()) {
				CCountry newCountry = countryData.get();
				newCountry.setCountryName(cCountry.getCountryName());
				newCountry.setCountryCode(cCountry.getCountryCode());
				newCountry.setRegions(cCountry.getRegions());//khi khai báo country thì kèm theo đối tượng regions
				CCountry savedCountry = countryRepository.save(newCountry);
				return new ResponseEntity<>(savedCountry, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to update specified Country: "+e.getCause().getCause().getMessage());
		}
	}
	
	@DeleteMapping("/countries/{id}")
	public ResponseEntity<Object> deleteCountryById(@PathVariable Long id) {
		try {
			Optional<CCountry> optional= countryRepository.findById(id);
			if (optional.isPresent()) {
				countryRepository.deleteById(id);
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}			
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/countries-count")
	public long countCountry() {
		return countryRepository.count();
	}
	
	@GetMapping("/countries/check/{id}")
	public boolean checkCountryById(@PathVariable Long id) {
		return countryRepository.existsById(id);
	}
	
	@GetMapping("/country/code/{code}")
	public CCountry getCountryByCode(@PathVariable String code) {
		return countryRepository.findByCountryCode(code);
	}
	
	@GetMapping("/country/containing-code/{code}")
	public CCountry getCountryByContainingCode(@PathVariable String code) {
		return countryRepository.findByCountryCodeContaining(code);
	}

	//Task70.30
	@GetMapping("/country/name/{name}")
	public CCountry getCountryByName(@PathVariable String name) {
		return countryRepository.findByCountryName(name);
	}
	//Pageable
	@GetMapping("/countries5")
	public ResponseEntity<List<CCountry>> getFiveCountries(@RequestParam(value = "page", defaultValue = "1") String page,
	@RequestParam(value = "size", defaultValue = "5") String size) {
		try {
			Pageable pageWithFiveElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
			List<CCountry> countryList = new ArrayList<CCountry>();
			countryRepository.findAll(pageWithFiveElements).forEach(countryList::add);
			return new ResponseEntity<>(countryList, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
